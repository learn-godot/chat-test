extends Panel

const DEFAULT_HOST = "127.0.0.1"
const DEFAULT_PORT = 52212


#### Network callbacks from SceneTree ####
var clientId = 0

# callback from SceneTree
func _player_connected(id):
	if (get_tree().is_network_server()):
		out("Client #" + str(id) + " connected");
		rpc_id(id, "clientOutText", "Hello client #" + str(id) + ". Welocome to server");
		rpc_id(id, "setClientId", id);
	

func _player_disconnected(id):

	if (get_tree().is_network_server()):
		out("Client #" + str(id) + " disconnected")
	else:
		out("Server disconnected")

# callback from SceneTree, only for clients (not server)
func _connected_ok():
	# will not use this one
	pass
	
# callback from SceneTree, only for clients (not server)	
func _connected_fail():

	out("Couldn't connect")
	
	get_tree().set_network_peer(null) #remove peer
	
	disableButtons(false)

func _server_disconnected():
	out("Server disconnected")
	

func disableButtons(status=false):
	get_node("btn_server").set_disabled(status)
	get_node("btn_client").set_disabled(status)
	get_node("input_text").editable = status
	


func _ready():
	# connect all the callbacks related to networking
	get_tree().connect("network_peer_connected",self,"_player_connected")
	get_tree().connect("network_peer_disconnected",self,"_player_disconnected")
	get_tree().connect("connected_to_server",self,"_connected_ok")
	get_tree().connect("connection_failed",self,"_connected_fail")

	out("Ready to Start")
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_input_text_text_entered(new_text):
	if new_text != "":
		get_node("input_text").clear()
		if !get_tree().is_network_server():
			rpc("sendTextToServer", clientId, new_text)
		else: 
			var outText = "Server say> " + new_text
			out(outText);
			rpc("clientOutText", outText);

func _on_btn_server_pressed():
	var host = NetworkedMultiplayerENet.new()
	host.set_compression_mode(NetworkedMultiplayerENet.COMPRESS_RANGE_CODER)
	var err = host.create_server(DEFAULT_PORT) # max: 1 peer, since it's a 2 players game
	if (err!=OK):
		#is another server running?
		out("Can't host, address in use.")
		return
		
	get_tree().set_network_peer(host)
	disableButtons(true)
	out("Starting server...")


func _on_btn_client_pressed():
	var ip = DEFAULT_HOST
	if (not ip.is_valid_ip_address()):
		out("IP address is invalid")
		return
	
	var host = NetworkedMultiplayerENet.new()
	host.set_compression_mode(NetworkedMultiplayerENet.COMPRESS_RANGE_CODER)
	host.create_client(ip,DEFAULT_PORT)
	get_tree().set_network_peer(host)
	
	disableButtons(true)
	
	out("Connecting...")

func out(text):
	var oldText = get_node("view_text").text
	get_node("view_text").text = oldText + text + "\n"
	

remote func setClientId(_client_id):
	clientId = _client_id

remote func sendTextToServer(id, text):
	if (get_tree().is_network_server()):
		var outText = "Client #" + str(id) + " say> " + text
		out(outText);
		rpc("clientOutText", outText);


remote func clientOutText(text):
	if (!get_tree().is_network_server()):
		out(text)